﻿FROM mcr.microsoft.com/dotnet/sdk:latest AS build-env
WORKDIR /app

COPY ./GitSyncPusher ./

RUN dotnet --info \
&& dotnet restore \
&& dotnet clean

RUN dotnet build --no-restore
RUN dotnet publish --no-restore -c Release -o out


FROM mcr.microsoft.com/dotnet/aspnet:latest
WORKDIR /app
COPY --from=build-env /app/out .

EXPOSE 5777
ENV ASPNETCORE_URLS=http://+:5777

ENTRYPOINT ["dotnet", "GitSyncPusher.dll"]
