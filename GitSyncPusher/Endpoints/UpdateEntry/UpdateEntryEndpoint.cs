using Ardalis.ApiEndpoints;
using GitSyncPusher.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace GitSyncPusher.Endpoints.UpdateEntry;

public class UpdateEntryEndpoint(ILogger<UpdateEntryEndpoint> logger, IUpdatePushEntry updatePushEntry)
    : EndpointBaseAsync.WithRequest<UpdateEntryRequest>.WithActionResult<string>
{
    [HttpPost("/api/update/pushEntry")]
    [SwaggerOperation(
        Summary = "update",
        Description = "update a push entry with credentials",
        OperationId = "8eeeb645-3965-4a71-949b-dd7d0e0ea3c9",
        Tags = new[] {"api"}
    )]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status304NotModified)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
    public override async Task<ActionResult<string>> HandleAsync([FromBody] UpdateEntryRequest request,
        CancellationToken cancellationToken = new CancellationToken())
    {
        logger.LogInformation("update project with id {ProjectId}", request.ProjectId);

        var result = await updatePushEntry.Update(request);
        return result.Status switch
        {
            200 => Ok(),
            404 => NotFound(result.Message),
            _ => UnprocessableEntity(result.Message)
        };
    }
}