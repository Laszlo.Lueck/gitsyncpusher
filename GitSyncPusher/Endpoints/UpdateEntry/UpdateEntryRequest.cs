using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc;

namespace GitSyncPusher.Endpoints.UpdateEntry;

public record UpdateEntryRequest(
    [property: JsonPropertyName("projectId"), JsonInclude]
    Guid ProjectId,
    [property: JsonPropertyName("name"), JsonInclude]
    string Name,
    [property: JsonPropertyName("active"), JsonInclude]
    bool Active,
    [property: JsonPropertyName("credential"), JsonInclude]
    UpdateCredentialRequest UpdateCredentialRequest
);