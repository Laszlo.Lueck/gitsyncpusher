﻿using System.Text.Json.Serialization;

namespace GitSyncPusher.Endpoints.UpdateEntry;

public record UpdateCredentialRequest(
    [property: JsonPropertyName("branchName"), JsonInclude]
    string BranchName,
    [property: JsonPropertyName("gitConfigUser"), JsonInclude]
    string GitConfigUser,
    [property: JsonPropertyName("gitConfigEmail"), JsonInclude]
    string GitConfigEmail,
    [property: JsonPropertyName("gitCredentialTokenName"), JsonInclude]
    string GitCredentialTokenName,
    [property: JsonPropertyName("gitCredentialToken"), JsonInclude]
    string GitCredentialToken,
    [property: JsonPropertyName("gitRepoRemote"), JsonInclude]
    string GitRepoRemote,
    [property: JsonPropertyName("gitRepoPushRefSpec"), JsonInclude]
    string GitRepoPushRefSpec,
    [property: JsonPropertyName("templateFileName"), JsonInclude]
    string TemplateFileName,
    [property: JsonPropertyName("gitProjectUrl"), JsonInclude]
    string GitProjectUrl,
    [property: JsonPropertyName("gitDirectoryName"), JsonInclude]
    string GitDirectoryName
);