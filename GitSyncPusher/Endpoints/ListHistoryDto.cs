using System.Text.Json.Serialization;
using GitSyncPusher.Database;

namespace GitSyncPusher.Endpoints;

public class ListHistoryDto
{
    [JsonPropertyName("projectId")] [JsonInclude]
    public Guid ProjectId;

    [JsonPropertyName("name")] [JsonInclude]
    public string? Name;

    [JsonPropertyName("_id")] [JsonInclude]
    public string? Id;

    [JsonPropertyName("createDate")] [JsonInclude]
    public DateTime CreateDate;

    [JsonPropertyName("historyDate")] [JsonInclude]
    public DateTime HistoryDate;

    [JsonPropertyName("lastChange")] [JsonInclude]
    public DateTime LastChange;

    [JsonPropertyName("lastDockerTag")] [JsonInclude]
    public string? LastDockerTag;

    [JsonPropertyName("directoryName")] [JsonInclude]
    public string? DirectoryName;
    
    public static implicit operator ListHistoryDto(PushEntryHistory pushEntryHistory) => new()
    {
        Name = pushEntryHistory.Name,
        ProjectId = pushEntryHistory.ProjectId,
        Id = pushEntryHistory.Id.ToString(),
        CreateDate = pushEntryHistory.CreateDate,
        HistoryDate = pushEntryHistory.HistoryDate,
        LastChange = pushEntryHistory.LastChange,
        LastDockerTag = pushEntryHistory.LastDockerTag,
        DirectoryName = pushEntryHistory.GitDirectoryName
    };
    
}