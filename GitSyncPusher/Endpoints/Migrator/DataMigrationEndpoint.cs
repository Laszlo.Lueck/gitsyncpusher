using Ardalis.ApiEndpoints;
using GitSyncPusher.Database;
using GitSyncPusher.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace GitSyncPusher.Endpoints.Migrator;

public class DataMigrationEndpoint : EndpointBaseAsync.WithoutRequest.WithActionResult
{
    private readonly ILogger<DataMigrationEndpoint> _logger;
    private readonly IStorageConnector<Database.PushEntry> _storageConnector;
    private readonly ICredentialConnector<Credential> _credentialConnector;
    private readonly IConfigurationHandler _configurationHandler;

    public DataMigrationEndpoint(ILogger<DataMigrationEndpoint> logger,
        IStorageConnector<Database.PushEntry> storageConnector,
        ICredentialConnector<Credential> credentialConnector,
        IConfigurationHandler configurationHandler)
    {
        _logger = logger;
        _storageConnector = storageConnector;
        _credentialConnector = credentialConnector;
        _configurationHandler = configurationHandler;
    }

    [HttpGet("/api/migrate")]
    [SwaggerOperation(
        Summary = "migrate",
        Description = "migrate data",
        OperationId = "f6962ae8-b244-4680-ba39-91a89d1ff79a",
        Tags = new[] {"api"}
    )]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public override async Task<ActionResult> HandleAsync(CancellationToken cancellationToken = new CancellationToken())
    {

        
        // var gitConfigOpt = GitConfigurationHandler.GetGitConfiguration(_configurationHandler);
        // const string projectUrl = "https://gitlab.com/Laszlo.Lueck/portainer.git";
        // const string templateFileName = "docker-compose_template.yaml";
        //
        // var r = await gitConfigOpt.MatchAsync(
        //     Some: async config =>
        //     {
        //         var tmpResult = await _storageConnector.ReadManyAsync();
        //         foreach (var pushEntry in tmpResult)
        //         {
        //             var credential = new Credential()
        //             {
        //                 BranchName = config.GitBranchName,
        //                 GitConfigUser = config.GitConfigUser,
        //                 GitRepoRemote = config.GitRepoRemote,
        //                 GitConfigEmail = config.GitConfigEmail,
        //                 GitCredentialToken = config.GitCredentialPassword,
        //                 GitCredentialTokenName = config.GitCredentialUser,
        //                 GitRepoPushRefSpec = config.GitRepoPushRefSpec,
        //                 ProjectId = pushEntry.ProjectId,
        //                 TemplateFileName = templateFileName,
        //                 CreateDate = DateTime.Now,
        //                 LastChangeDate = DateTime.Now,
        //                 GitProjectUrl = projectUrl
        //             };
        //
        //             _logger.LogInformation("writing {Name}", pushEntry.Name);
        //             await _credentialConnector.InsertCredential(credential);
        //         }
        //
        //         return 1;
        //     },
        //     None: () =>
        //     {
        //         _logger.LogWarning("nothing to do, no configuration found");
        //         return -1;
        //     });
        //
        // return Ok(r);
        return await Task.Run(Ok, cancellationToken);
    }
}