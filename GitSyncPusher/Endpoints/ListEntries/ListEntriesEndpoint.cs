using Ardalis.ApiEndpoints;
using GitSyncPusher.Database;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace GitSyncPusher.Endpoints.ListEntries;

public class ListEntriesEndpoint(ILogger<ListEntriesEndpoint> logger,
        IStorageConnector<Database.PushEntry> databaseConnector)
    : EndpointBaseAsync.WithoutRequest.WithActionResult<IEnumerable<ListEntryResult>>
{
    [HttpGet("/api/list")]
    [SwaggerOperation(
        Summary = "get pushes",
        Description = "get list of all pushrequests",
        OperationId = "ec0d62b4-ef18-4e5c-9885-90881b4891b",
        Tags = new[] { "api" }
    )]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public override async Task<ActionResult<IEnumerable<ListEntryResult>>> HandleAsync(
        CancellationToken cancellationToken = new CancellationToken())
    {
        logger.LogInformation("get all entries from database");
        var retResult = await databaseConnector.ReadManyAsync();
        return Ok(retResult.Map(d => (ListEntryResult)d));
    }
}