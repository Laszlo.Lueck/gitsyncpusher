using System.Text.Json.Serialization;
using GitSyncPusher.Database;

namespace GitSyncPusher.Endpoints.ListEntries;

public class ListEntryResult
{
    [JsonPropertyName("projectId"), JsonInclude] public Guid ProjectId;

    [JsonPropertyName("name"), JsonInclude]
    public string? Name;

    [JsonPropertyName("createDate"), JsonInclude]
    public DateTime CreateDate;

    [JsonPropertyName("lastChange"), JsonInclude]
    public DateTime LastChange;

    [JsonPropertyName("lastDockerTag"), JsonInclude]
    public string? LastDockerTag;

    [JsonPropertyName("active"), JsonInclude] public bool Active;

    public static implicit operator ListEntryResult(Database.PushEntry entry) => new()
    {
        ProjectId = entry.ProjectId, Name = entry.Name, LastChange = entry.LastChange,
        CreateDate = entry.CreateDate, LastDockerTag = entry.LastDockerTag, Active = entry.IsActiveEntity
    };
}