using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace GitSyncPusher.Endpoints;

public class ListHistoryBaseRequest
{
    [FromRoute(Name = "page")]
    [Required]
    [Range(1, 100)]
    public int Page { get; set; }

    [FromRoute(Name = "pageSize")]
    [Required]
    [Range(1, double.MaxValue)]
    public int PageSize { get; set; }
}