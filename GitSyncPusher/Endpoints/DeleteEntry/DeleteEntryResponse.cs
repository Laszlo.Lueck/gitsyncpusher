using System.Text.Json.Serialization;
using GitSyncPusher.Services;

namespace GitSyncPusher.Endpoints.DeleteEntry;

public record DeleteEntryResponse(
    [property: JsonPropertyName("projectId"), JsonInclude]
    Guid ProjectId,
    [property: JsonPropertyName("pushEntrySuccess"), JsonInclude]
    bool PushEntrySuccess,
    [property: JsonPropertyName("credentialSuccess"), JsonInclude]
    bool CredentialSuccess,
    [property: JsonPropertyName("deletedHistories"), JsonInclude]
    long DeletedHistories,
    [property: JsonPropertyName("hasError"), JsonInclude]
    bool HasError,
    [property: JsonPropertyName("errorMessage"), JsonInclude]
    string? ErrorMessage
)
{
    public static implicit operator DeleteEntryResponse(DeleteEntryResult deleteEntryResult) => new(
        ProjectId: deleteEntryResult.ProjectId, PushEntrySuccess: deleteEntryResult.PushEntrySuccess,
        CredentialSuccess: deleteEntryResult.CredentialSuccess, DeletedHistories: deleteEntryResult.DeletedHistories,
        HasError: deleteEntryResult.HasError, ErrorMessage: deleteEntryResult.ErrorMessage);
}