using System.Net.Mime;
using Ardalis.ApiEndpoints;
using GitSyncPusher.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace GitSyncPusher.Endpoints.DeleteEntry;

public class DeleteEntryEndpoint(ILogger<DeleteEntryEndpoint> logger, IDeletePushEntry deletePushEntry)
    : EndpointBaseAsync.WithRequest<Guid>.WithActionResult<DeleteEntryResponse>
{
    [HttpDelete("/api/delete/{projectId}")]
    [SwaggerOperation(
        Summary = "remove push entry",
        Description = "remove a push entry by given projectId",
        OperationId = "a70aeddb-ac07-4ec9-8d12-8194198c8f4f",
        Tags = new[] {"api"}
    )]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
    public override async Task<ActionResult<DeleteEntryResponse>> HandleAsync(Guid projectId,
        CancellationToken cancellationToken = new())
    {
        logger.LogInformation("try to delete PushEntry with projectId {ProjectId}", projectId);
        DeleteEntryResponse deleteResult = await deletePushEntry.ProcessDeletePushEntry(projectId);
        return (deleteResult.HasError, deleteResult.CredentialSuccess, deleteResult.PushEntrySuccess) switch
        {
            (false, false, false) => NotFound(deleteResult),
            (true, _, _) => UnprocessableEntity(deleteResult),
            _ => Ok(deleteResult)
        };
    }
}