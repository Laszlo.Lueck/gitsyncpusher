using Ardalis.ApiEndpoints;
using GitSyncPusher.Endpoints.PushEntry;
using GitSyncPusher.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace GitSyncPusher.Endpoints.RemotePushEntry;

public class PushEntryEndpoint(IConfigurationHandler configurationHandler, ILogger<PushEntryEndpoint> logger,
        IGitHandler gitHandler)
    : EndpointBaseAsync.WithRequest<PushEntryRequest>.WithActionResult
{
    [HttpPost("/api/push/{projectId}/{dockerTag}")]
    [SwaggerOperation(
        Summary = "receive push request",
        Description = "receive a push request and update the appropriate docker-compose file in git",
        OperationId = "4e72e9f4-aa22-49dd-9633-e9b53c1aff2e",
        Tags = new[] {"api"}
    )]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status304NotModified)]
    public override async Task<ActionResult> HandleAsync([FromRoute] PushEntryRequest request,
        CancellationToken cancellationToken = new())
    {
        if (string.IsNullOrEmpty(request.DockerTag))
        {
            logger.LogWarning("cannot process message for processId {ProcessId}, there is no docker tag in request",
                request.ProjectId);
            return NoContent();
        }

        var gitConfigOpt = GitConfigurationHandler.GetGitConfiguration(configurationHandler);

        return await gitConfigOpt.Match(
            Some: async gitConfig =>
            {
                var boolResult = await gitHandler.Process(gitConfig, request.ProjectId, request.DockerTag);
                if (!boolResult)
                    logger.LogWarning(
                        "something went wrong while processing with git, see prior logs for more information");
                return (ActionResult) NoContent();
            },
            None: async () =>
            {
                return await Task.Run(() =>
                {
                    logger.LogWarning(
                        "cannot process job, one ore more configuration elements are missing or malformed, see prior logs for more info");
                    return (ActionResult) Ok("some configuration elements are missing, see log for details");
                }, cancellationToken);
            });
    }
}