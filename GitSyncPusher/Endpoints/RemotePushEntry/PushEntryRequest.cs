using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc;

namespace GitSyncPusher.Endpoints.PushEntry;

public class PushEntryRequest
{
    [FromRoute(Name = "projectId")] public Guid ProjectId { get; set; }

    [FromRoute(Name = "dockerTag")] public string? DockerTag { get; set; }
}