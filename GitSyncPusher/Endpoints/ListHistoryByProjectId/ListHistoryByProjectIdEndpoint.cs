﻿using Ardalis.ApiEndpoints;
using GitSyncPusher.Database;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace GitSyncPusher.Endpoints.ListHistoryByProjectId;

public class ListHistoryByProjectIdEndpoint(ILogger<ListHistoryByProjectIdEndpoint> logger,
        IStorageHistoryConnector<PushEntryHistory> storageHistoryConnector)
    : EndpointBaseAsync.WithRequest<ListHistoryByProjectIdRequest>.
    WithActionResult<ListHistoryByProjectIdResult>
{
    [HttpGet("/api/history/list/projectId/{projectId}/{page}/{pageSize}")]
    [SwaggerOperation(
        Summary = "list all history by projectId",
        Description = "list all history elements with projectId filter",
        OperationId = "4ae077f2-c1db-456d-ba72-70d977a2327c",
        Tags = new[] {"api", "history"}
    )]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public override async Task<ActionResult<ListHistoryByProjectIdResult>> HandleAsync(
        [FromRoute] ListHistoryByProjectIdRequest request, CancellationToken cancellationToken = new())
    {
        logger.LogInformation("try to get all history elements filtered by project id {ProjectId}", request.ProjectId);

        var result =
            await storageHistoryConnector.GetByProjectIdAsync(request.ProjectId, request.Page, request.PageSize);
        var returnResult = new ListHistoryByProjectIdResult
        {
            DocCount = result.TotalDocCount,
            PageCount = result.TotalPageCount,
            DataList = result.Data.Map(d => (ListHistoryDto) d)
        };

        return returnResult;
    }
}