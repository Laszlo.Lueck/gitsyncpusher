using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc;

namespace GitSyncPusher.Endpoints.ListHistoryByProjectId;

public class ListHistoryByProjectIdRequest : ListHistoryBaseRequest
{
    [FromRoute(Name = "projectId")]
    [Required]
    public Guid ProjectId {get; set; }
}