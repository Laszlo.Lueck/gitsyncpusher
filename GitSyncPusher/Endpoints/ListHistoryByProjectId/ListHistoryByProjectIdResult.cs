﻿using System.Text.Json.Serialization;
using GitSyncPusher.Endpoints.ListHistoryAll;

namespace GitSyncPusher.Endpoints.ListHistoryByProjectId;

public class ListHistoryByProjectIdResult
{
    [JsonPropertyName("pageCount")] [JsonInclude]
    public int PageCount;

    [JsonPropertyName("docCount")] [JsonInclude]
    public long DocCount;

    [JsonPropertyName("dataList")] [JsonInclude]
    public IEnumerable<ListHistoryDto>? DataList;
}