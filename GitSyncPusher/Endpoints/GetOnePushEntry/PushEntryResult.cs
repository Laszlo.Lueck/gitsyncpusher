using System.Text.Json.Serialization;

namespace GitSyncPusher.Endpoints.GetOnePushEntry;

public class PushEntryResult
{
    [JsonPropertyName("projectId"), JsonInclude]
    public Guid ProjectId;

    [JsonPropertyName("name"), JsonInclude]
    public string? Name;

    [JsonPropertyName("createDate"), JsonInclude]
    public DateTime CreateDate;

    [JsonPropertyName("lastChange"), JsonInclude]
    public DateTime LastChange;

    [JsonPropertyName("lastDockerTag"), JsonInclude]
    public string? LastDockerTag;

    [JsonPropertyName("active"), JsonInclude]
    public bool Active;

    [JsonPropertyName("credential"), JsonInclude]
    public CredentialResult? CredentialDto;

    public static implicit operator PushEntryResult((Database.PushEntry entry, CredentialResult dto) tuple) => new()
    {
        ProjectId = tuple.entry.ProjectId,
        Name = tuple.entry.Name,
        LastChange = tuple.entry.LastChange,
        CreateDate = tuple.entry.CreateDate,
        LastDockerTag = tuple.entry.LastDockerTag,
        Active = tuple.entry.IsActiveEntity,
        CredentialDto = tuple.dto
    };
}