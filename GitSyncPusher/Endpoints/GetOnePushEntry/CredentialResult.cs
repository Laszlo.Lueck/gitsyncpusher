using System.Text.Json.Serialization;
using GitSyncPusher.Database;

namespace GitSyncPusher.Endpoints;

public class CredentialResult
{
    [JsonPropertyName("branchName"), JsonInclude]
    public string? BranchName;

    [JsonPropertyName("gitConfigUser"), JsonInclude]
    public string? GitConfigUser;

    [JsonPropertyName("gitConfigEmail"), JsonInclude]
    public string? GitConfigEmail;

    [JsonPropertyName("gitCredentialTokenName"), JsonInclude]
    public string? GitCredentialTokenName;

    [JsonPropertyName("gitCredentialToken"), JsonInclude]
    public string? GitCredentialToken;

    [JsonPropertyName("gitRepoRemote"), JsonInclude]
    public string? GitRepoRemote;

    [JsonPropertyName("gitRepoPushRefSpec"), JsonInclude]
    public string? GitPushRefSpec;

    [JsonPropertyName("templateFileName"), JsonInclude]
    public string? TemplateFileName;

    [JsonPropertyName("createDate"), JsonInclude]
    public DateTime CreateDate;

    [JsonPropertyName("lastChangeDate"), JsonInclude]
    public DateTime LastChangeDate;

    [JsonPropertyName("gitProjectUrl"), JsonInclude]
    public string? GitProjectUrl;

    [JsonPropertyName("gitDirectoryName"), JsonInclude]
    public string? GitDirectoryName;

    public static implicit operator CredentialResult(Credential credential) => new()
    {
        BranchName = credential.BranchName,
        GitConfigEmail = credential.GitConfigEmail,
        GitConfigUser = credential.GitConfigUser,
        GitCredentialToken = credential.GitCredentialToken,
        GitCredentialTokenName = credential.GitCredentialTokenName,
        GitPushRefSpec = credential.GitRepoPushRefSpec,
        GitRepoRemote = credential.GitRepoRemote,
        TemplateFileName = credential.TemplateFileName,
        CreateDate = credential.CreateDate,
        LastChangeDate = credential.LastChangeDate,
        GitProjectUrl = credential.GitProjectUrl,
        GitDirectoryName = credential.GitDirectoryName
    };


}