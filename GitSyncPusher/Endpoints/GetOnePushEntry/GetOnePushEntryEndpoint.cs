using Ardalis.ApiEndpoints;
using GitSyncPusher.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using static LanguageExt.Prelude;

namespace GitSyncPusher.Endpoints.GetOnePushEntry;

public class GetOnePushEntryEndpoint(ILogger<GetOnePushEntryEndpoint> logger, IGetOnePushEntry getOnePushEntry)
    : EndpointBaseAsync.WithRequest<Guid>.WithActionResult<PushEntryResult>
{
    [HttpGet("/api/get/{projectId}")]
    [SwaggerOperation(
        Summary = "get push entry",
        Description = "get a push entry by given projectId",
        OperationId = "4cbc2642-ac6c-4c6e-9c87-c99188b7394f",
        Tags = new[] {"api"}
    )]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public override async Task<ActionResult<PushEntryResult>> HandleAsync(Guid projectId,
        CancellationToken cancellationToken = new CancellationToken())
    {
        logger.LogInformation("try to get an push entry from a project id {ProjectId}", projectId);

        var getOneResultOpt = await getOnePushEntry.GetOnePushEntryOptionAsync(projectId);

        return match(
            getOneResultOpt,
            Some: result => (ActionResult) Ok((PushEntryResult) result),
            None:
            () => (ActionResult) NotFound(
                $"cannot found pushEntry or credential for projectId {projectId.ToString()}"));
    }
}