using System.Text.Json.Serialization;

namespace GitSyncPusher.Endpoints.CreateEntry;

public record CreateEntryRequest(
    [property: JsonPropertyName("active"), JsonInclude]
    bool Active,
    [property: JsonPropertyName("name"), JsonInclude]
    string Name,
    [property: JsonPropertyName("credential"), JsonInclude]
    CreateCredentialRequest CreateCredentialRequest
);