using System.Text.Json.Serialization;
using GitSyncPusher.Services;

namespace GitSyncPusher.Endpoints.CreateEntry;

public record CreateEntryResponse(
    [property: JsonPropertyName("id"), JsonInclude]
    Guid? Id,
    [property: JsonPropertyName("created"), JsonInclude]
    DateTime? Created,
    [property: JsonPropertyName("hasError"), JsonInclude]
    bool HasError,
    [property: JsonPropertyName("errorMessage"), JsonInclude]
    string? ErrorMessage
)
{
    public static implicit operator CreateEntryResponse(CreatePushEntryResult createPushEntryResult) => new(
        ErrorMessage: createPushEntryResult.ErrorMessage, Created: createPushEntryResult.CreateDate,
        HasError: createPushEntryResult.HasError, Id: createPushEntryResult.Guid
    );
}