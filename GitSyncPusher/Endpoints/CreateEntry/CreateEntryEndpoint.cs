using Ardalis.ApiEndpoints;
using GitSyncPusher.Services;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace GitSyncPusher.Endpoints.CreateEntry;

public class
    CreateEntryEndpoint(ILogger<CreateEntryEndpoint> logger, ICreatePushEntry createPushEntry)
    : EndpointBaseAsync.WithRequest<CreateEntryRequest>.WithActionResult<CreateEntryResponse>
{
    [HttpPut("api/create/pushentry")]
    [SwaggerOperation(
        Summary = "create push",
        Description = "create a push for processing",
        OperationId = "6aa5d96d-830a-469b-b6c8-2a84ee9a657e",
        Tags = new[] {"api"}
    )]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
    public override async Task<ActionResult<CreateEntryResponse>> HandleAsync(CreateEntryRequest request,
        CancellationToken cancellationToken = new())
    {
        if (string.IsNullOrEmpty(request.Name))
        {
            logger.LogWarning("request property name ist missing");
            var responseEntry = new CreateEntryResponse
                (ErrorMessage: "request property name is missing", HasError: true, Created: null, Id: null);
            return UnprocessableEntity(responseEntry);
        }

        var dto = new NewPushEntryDto(
            Name: request.Name,
            Active: request.Active,
            CredentialDto: request.CreateCredentialRequest
        );


        CreateEntryResponse createPushEntryResult = await createPushEntry.ProcessNewPushEntry(dto);
        if (createPushEntryResult.HasError)
            return Conflict(createPushEntryResult);

        return Ok(createPushEntryResult);
    }
}