using System.Text.Json.Serialization;

namespace GitSyncPusher.Endpoints.ListHistoryAll;

public class ListHistoryAllResult
{
    [JsonPropertyName("pageCount")] [JsonInclude]
    public int PageCount;

    [JsonPropertyName("docCount")] [JsonInclude]
    public long DocCount;

    [JsonPropertyName("dataList")] [JsonInclude]
    public IEnumerable<ListHistoryDto>? DataList;
}