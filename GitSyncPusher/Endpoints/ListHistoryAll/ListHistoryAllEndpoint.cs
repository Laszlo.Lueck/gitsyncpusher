using Ardalis.ApiEndpoints;
using GitSyncPusher.Database;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace GitSyncPusher.Endpoints.ListHistoryAll;

public class
    ListHistoryAllEndpoint(ILogger<ListHistoryAllEndpoint> logger,
        IStorageHistoryConnector<PushEntryHistory> dataConnector)
    : EndpointBaseAsync.WithRequest<ListHistoryAllRequest>.WithActionResult<ListHistoryAllResult>
{
    [HttpGet("/api/history/list/{page}/{pageSize}")]
    [SwaggerOperation(
        Summary = "list all history",
        Description = "list all history elements without filter",
        OperationId = "79e9cded-0d57-4328-84b3-f0df12e8740a",
        Tags = new[] {"api", "history"}
    )]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
    public override async Task<ActionResult<ListHistoryAllResult>> HandleAsync(
        [FromRoute] ListHistoryAllRequest request,
        CancellationToken cancellationToken = new())
    {
        logger.LogInformation("try to get all history elements without filter");
        var elements = await dataConnector.GetAllAsync(request.Page, request.PageSize);

        var returnResult = new ListHistoryAllResult
        {
            PageCount = elements.TotalPageCount,
            DocCount = elements.TotalDocCount,
            DataList = elements.Data.Map(element => (ListHistoryDto) element)
        };
        return Ok(returnResult);
    }
}