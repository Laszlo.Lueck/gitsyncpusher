namespace GitSyncPusher.Exceptions;

[Serializable]
public class InactiveEntityException : Exception
{
    public InactiveEntityException()
    {
        
    }
    
    public InactiveEntityException(string message) : base(message) {}
}