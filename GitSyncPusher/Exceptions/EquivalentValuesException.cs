﻿namespace GitSyncPusher.Exceptions;

[Serializable]
public class EquivalentValuesException : Exception
{
    public EquivalentValuesException()
    {
        
    }

    public EquivalentValuesException(string message) : base(message) {}

}