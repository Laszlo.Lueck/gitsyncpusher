﻿using LanguageExt;

namespace GitSyncPusher.Database;

public interface ICredentialConnector<T>
{

    Task<Option<T>> GetCredentialByProjectId(Guid projectId);

    Task<Option<T>> GetCredentialByProjectName(string gitDirectoryName);

    Task<long> DeleteCredentialByProjectId(Guid projectId);

    Task<int> InsertCredential(T value);

    Task<long> UpdateCredential(T value);
}