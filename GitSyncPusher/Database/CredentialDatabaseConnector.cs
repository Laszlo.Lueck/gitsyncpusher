﻿using GitSyncPusher.Services;
using LanguageExt;
using MethodTimer;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace GitSyncPusher.Database;

public class CredentialDatabaseConnector : ICredentialConnector<Credential>
{
    private readonly ILogger<CredentialDatabaseConnector> _logger;
    private readonly IMongoCollection<Credential> _mongoCollection;

    public CredentialDatabaseConnector(ILogger<CredentialDatabaseConnector> logger,
        IOptions<PushEntryDatabaseSettings> databaseSettings)
    {
        _logger = logger;
        var mongoClient = new MongoClient(databaseSettings.Value.PushEntryConnectionString);
        var mongoDatabase = mongoClient.GetDatabase(databaseSettings.Value.PushEntryDatabaseName);
        _mongoCollection =
            mongoDatabase.GetCollection<Credential>(databaseSettings.Value.StoredCredentialsCollectionName);
    }

    [Time]
    public async Task<Option<Credential>> GetCredentialByProjectId(Guid projectId)
    {
        var resultOpt = await _mongoCollection.FindAsync(f => f.ProjectId == projectId);
        return resultOpt.FirstOrDefault();
    }

    [Time]
    public async Task<Option<Credential>> GetCredentialByProjectName(string gitDirectoryName)
    {
        var result = await _mongoCollection.FindAsync(f => f.GitDirectoryName == gitDirectoryName);
        return result.FirstOrDefault();
    }

    [Time]
    public async Task<long> DeleteCredentialByProjectId(Guid projectId)
    {
        try
        {
            var deleteResult = await _mongoCollection.DeleteOneAsync(f => f.ProjectId == projectId);
            return deleteResult.DeletedCount;
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "an error occured");
            return -1;
        }
    }

    [Time]
    public async Task<int> InsertCredential(Credential value)
    {
        try
        {
            await _mongoCollection.InsertOneAsync(value);
            return 1;
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "an error occured");
            return 0;
        }
    }

    [Time]
    public async Task<long> UpdateCredential(Credential value)
    {
        try
        {
            var updateResult = await _mongoCollection.ReplaceOneAsync(f => f.ProjectId == value.ProjectId, value);
            return updateResult.ModifiedCount;
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "an error occured");
            return -1;
        }
    }
}