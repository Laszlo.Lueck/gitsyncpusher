using GitSyncPusher.Services;
using MethodTimer;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace GitSyncPusher.Database;

public class PushEntryHistoryDatabaseConnector : IStorageHistoryConnector<PushEntryHistory>
{
    private readonly ILogger<PushEntryHistoryDatabaseConnector> _logger;
    private readonly IMongoCollection<PushEntryHistory> _mongoCollection;

    public PushEntryHistoryDatabaseConnector(ILogger<PushEntryHistoryDatabaseConnector> logger,
        IOptions<PushEntryDatabaseSettings> databaseSettings)
    {
        _logger = logger;
        var mongoClient = new MongoClient(databaseSettings.Value.PushEntryConnectionString);
        var mongoDatabase = mongoClient.GetDatabase(databaseSettings.Value.PushEntryDatabaseName);
        _mongoCollection =
            mongoDatabase.GetCollection<PushEntryHistory>(databaseSettings.Value.PushEntryHistoryCollectionName);
    }

    [Time]
    public async Task<(int TotalPageCount, long TotalDocCount, IEnumerable<PushEntryHistory> Data)> GetAllAsync(
        int page, int pageSize)
    {
        _logger.LogInformation("try to read all data with pagination");
        var filter = Builders<PushEntryHistory>.Filter.Empty;
        var sort = Builders<PushEntryHistory>.Sort.Descending(x => x.HistoryDate);
        return await AggregateResult(_logger, _mongoCollection, filter, sort, page, pageSize);
    }

    [Time]
    public async Task<(int TotalPageCount, long TotalDocCount, IEnumerable<PushEntryHistory> Data)> GetByProjectIdAsync(
        Guid projectId, int page,
        int pageSize)
    {
        _logger.LogInformation("try to get all data by project {ProjectId}", projectId);
        var filter = Builders<PushEntryHistory>.Filter.Eq(d => d.ProjectId, projectId);
        var sort = Builders<PushEntryHistory>.Sort.Descending(x => x.HistoryDate);
        return await AggregateResult(_logger, _mongoCollection, filter, sort, page, pageSize);
    }

    private static readonly Func<ILogger, IMongoCollection<PushEntryHistory>, FilterDefinition<PushEntryHistory>,
        SortDefinition<PushEntryHistory>, int, int, Task<
            (int TotalPageCount, long TotalDocCount,
            IEnumerable<PushEntryHistory> Data)>> AggregateResult =
        async (logger, mongoCollection, filter, sort, page, pageSize) =>
        {
            var countFacet = AggregateFacet.Create("count",
                PipelineDefinition<PushEntryHistory, AggregateCountResult>.Create(new[]
                {
                    PipelineStageDefinitionBuilder.Count<PushEntryHistory>()
                }));

            var dataFacet = AggregateFacet.Create("data",
                PipelineDefinition<PushEntryHistory, PushEntryHistory>.Create(new[]
                {
                    PipelineStageDefinitionBuilder.Sort(sort),
                    PipelineStageDefinitionBuilder.Skip<PushEntryHistory>((page - 1) * pageSize),
                    PipelineStageDefinitionBuilder.Limit<PushEntryHistory>(pageSize),
                }));

            var aggregation = await mongoCollection
                .Aggregate()
                .Match(filter)
                .Facet(countFacet, dataFacet)
                .ToListAsync();

            var count = aggregation
                .First()
                .Facets
                .First(x => x.Name == "count")
                .Output<AggregateCountResult>()[0]
                ?.Count ?? 0;

            var totalPages = (int) Math.Ceiling((double) count / pageSize);

            logger.LogInformation("receive a message count {MessageCount}", count);
            logger.LogInformation("get with page {Page}", page);
            logger.LogInformation("get with pageSize {PageSize}", pageSize);
            logger.LogInformation("calculated totalPages {TotalPages}", totalPages);

            var data = aggregation.First()
                .Facets.First(x => x.Name == "data")
                .Output<PushEntryHistory>();

            return (totalPages, count, data);
        };

    [Time]
    public async Task<int> WriteOneAsync(PushEntryHistory value)
    {
        try
        {
            _logger.LogInformation("write a history entry to database");
            await _mongoCollection.InsertOneAsync(value);
            return 1;
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "an error occured");
            return 0;
        }
    }

    [Time]
    public async Task<long> DeleteAllByProjectId(Guid projectId)
    {
        try
        {
            _logger.LogInformation("remove all history events for projectId {ProjectId}", projectId);
            var returnResult = await _mongoCollection.DeleteManyAsync(d => d.ProjectId == projectId);
            return returnResult.DeletedCount;
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "an error occured");
            return -1;
        }
    }
}