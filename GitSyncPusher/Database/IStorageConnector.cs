using LanguageExt;

namespace GitSyncPusher.Database;

public interface IStorageConnector<T>
{
    Task<IEnumerable<T>> ReadManyAsync();

    Task<Option<T>> GetByProjectIdAsync(Guid id);
    
    Task<int> WriteOneAsync(T value);

    Task<long> DeleteOneAsync(Guid projectId);

    Task<long> UpdateOneAsync(T value);
}