using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GitSyncPusher.Database;

public class PushEntry
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public ObjectId Id { get; set; }
    
    [BsonElement("projectId")]
    public Guid ProjectId { get; set; }
    
    [BsonElement("name")]
    public string? Name { get; set; }
    
    [BsonElement("createDate")]
    public DateTime CreateDate { get; set; }
    
    [BsonElement("lastChange")]
    public DateTime LastChange { get; set; }
    
    [BsonElement("lastDockerTag")]
    public string? LastDockerTag { get; set; }
    
    [BsonElement("isActiveEntity")]
    public bool IsActiveEntity { get; set; }
    
}