﻿using GitSyncPusher.Endpoints;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GitSyncPusher.Database;

public class Credential
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public ObjectId Id { get; set; }

    [BsonElement("projectId")] public Guid ProjectId { get; set; }

    [BsonElement("branchName")] public string? BranchName { get; set; }

    [BsonElement("gitConfigUser")] public string? GitConfigUser { get; set; }

    [BsonElement("gitConfigEmail")] public string? GitConfigEmail { get; set; }

    [BsonElement("gitCredentialTokenName")]
    public string? GitCredentialTokenName { get; set; }

    [BsonElement("gitCredentialToken")] public string? GitCredentialToken { get; set; }

    [BsonElement("gitRepoRemote")] public string? GitRepoRemote { get; set; }

    [BsonElement("gitRepoPushRefSpec")] public string? GitRepoPushRefSpec { get; set; }

    [BsonElement("templateFileName")] public string? TemplateFileName { get; set; }

    [BsonElement("createDate")] public DateTime CreateDate { get; set; }

    [BsonElement("lastChangeDate")] public DateTime LastChangeDate { get; set; }

    [BsonElement("gitProjectUrl")] public string? GitProjectUrl { get; set; }

    [BsonElement("gitDirectoryName")] public string? GitDirectoryName { get; set; }
    
}