namespace GitSyncPusher.Database;

public interface IStorageHistoryConnector<T>
{
    Task<(int TotalPageCount, long TotalDocCount, IEnumerable<T> Data)> GetByProjectIdAsync(Guid projectId, int position, int length);
    
    Task<(int TotalPageCount, long TotalDocCount, IEnumerable<T> Data)> GetAllAsync(int page, int pageSize);

    Task<int> WriteOneAsync(T value);

    Task<long> DeleteAllByProjectId(Guid projectId);

}