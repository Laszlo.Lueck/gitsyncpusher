using GitSyncPusher.Services;
using LanguageExt;
using MethodTimer;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace GitSyncPusher.Database;

public class PushEntryDatabaseConnector : IStorageConnector<PushEntry>
{
    private readonly ILogger<PushEntryDatabaseConnector> _logger;
    private readonly IMongoCollection<PushEntry> _mongoCollection;

    public PushEntryDatabaseConnector(ILogger<PushEntryDatabaseConnector> logger,
        IOptions<PushEntryDatabaseSettings> databaseSettings)
    {
        _logger = logger;
        var mongoClient = new MongoClient(databaseSettings.Value.PushEntryConnectionString);
        var mongoDatabase = mongoClient.GetDatabase(databaseSettings.Value.PushEntryDatabaseName);
        _mongoCollection = mongoDatabase.GetCollection<PushEntry>(databaseSettings.Value.PushEntryCollectionName);
    }

    [Time]
    public async Task<IEnumerable<PushEntry>> ReadManyAsync()
    {
        return (await _mongoCollection.FindAsync(_ => true)).ToEnumerable();
    }
    

    [Time]
    public async Task<Option<PushEntry>> GetByProjectIdAsync(Guid id)
    {
        var resultOpt = await _mongoCollection.FindAsync(f => f.ProjectId == id);
        return resultOpt.FirstOrDefault();
    }

    [Time]
    public async Task<int> WriteOneAsync(PushEntry value)
    {
        try
        {
            await _mongoCollection.InsertOneAsync(value);
            return 1;
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "an error occured");
            return 0;
        }
    }


    [Time]
    public async Task<long> DeleteOneAsync(Guid projectId)
    {
        try
        {
            var deleteResult = await _mongoCollection.DeleteOneAsync(f => f.ProjectId == projectId);
            return deleteResult.DeletedCount;
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "an error occured");
            return -1;
        }
    }

    [Time]
    public async Task<long> UpdateOneAsync(PushEntry value)
    {
        try
        {
            var updateResult = await _mongoCollection.ReplaceOneAsync(f => f.ProjectId == value.ProjectId, value);
            return updateResult.ModifiedCount;
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "an error occured");
            return -1;
        }
    }
}