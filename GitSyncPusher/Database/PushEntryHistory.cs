using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace GitSyncPusher.Database;

public class PushEntryHistory
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public ObjectId Id { get; set; }

    [BsonElement("projectId")] public Guid ProjectId { get; set; }

    [BsonElement("name")] public string? Name { get; set; }

    [BsonElement("createDate")] public DateTime CreateDate { get; set; }

    [BsonElement("lastChange")] public DateTime LastChange { get; set; }

    [BsonElement("lastDockerTag")] public string? LastDockerTag { get; set; }

    [BsonElement("gitDirectoryName")] public string? GitDirectoryName { get; set; }

    [BsonElement("historyDate")] public DateTime HistoryDate { get; set; }

    public static implicit operator PushEntryHistory((PushEntry pushEntry, string gitDirectoryName) valueTuple) => new()
    {
        CreateDate = valueTuple.pushEntry.CreateDate, GitDirectoryName = valueTuple.gitDirectoryName, Name = valueTuple.pushEntry.Name,
        LastChange = valueTuple.pushEntry.LastChange, LastDockerTag = valueTuple.pushEntry.LastDockerTag, Id = ObjectId.GenerateNewId(),
        ProjectId = valueTuple.pushEntry.ProjectId, HistoryDate = DateTime.Now
    };
}