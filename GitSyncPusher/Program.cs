using GitSyncPusher.Database;
using GitSyncPusher.Services;
using GitSyncPusher.Telemetry;
using HealthChecks.UI.Client;
using LanguageExt;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging.Console;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateSlimBuilder(args);
const string myAllowSpecificOrigins = "_myAllowSpecificOrigins";
// Add services to the container.

builder.Logging.AddSimpleConsole(options =>
{
    options.IncludeScopes = false;
    options.TimestampFormat = "[yyy-MM-dd HH:mm:ss.ffff]";
    options.ColorBehavior = LoggerColorBehavior.Enabled;
    options.SingleLine = true;
});
builder.Logging.AddFilter("*", LogLevel.Information);
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: myAllowSpecificOrigins,
        policy =>
        {
            policy
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
        });
});
builder.Services.AddControllers();
builder.Services.AddSingleton<IConfigurationHandler, ConfigurationHandler>();
builder.Services.Configure<PushEntryDatabaseSettings>(d =>
{
    d.PushEntryConnectionString = builder.Configuration.GetValue<string>("MONGODB_CONNECTION");
    d.PushEntryCollectionName = builder.Configuration.GetValue<string>("MONGODB_COLLECTION");
    d.PushEntryDatabaseName = builder.Configuration.GetValue<string>("MONGODB_DATABASE");
    d.PushEntryHistoryCollectionName = builder.Configuration.GetValue<string>("MONGODB_COLLECTION_HISTORY");
    d.StoredCredentialsCollectionName = builder.Configuration.GetValue<string>("MONGODB_COLLECTION_STORED_CREDENTIALS");
});
builder.Services.AddSingleton<IStorageConnector<PushEntry>, PushEntryDatabaseConnector>();
builder.Services.AddSingleton<IStorageHistoryConnector<PushEntryHistory>, PushEntryHistoryDatabaseConnector>();
builder.Services.AddSingleton<ICredentialConnector<Credential>, CredentialDatabaseConnector>();
builder.Services.AddSingleton<IGitHandler, GitHandler>();
builder.Services.AddSingleton<ICreatePushEntry, CreatePushEntry>();
builder.Services.AddSingleton<IUpdatePushEntry, UpdatePushEntry>();
builder.Services.AddSingleton<IDeletePushEntry, DeletePushEntry>();
builder.Services.AddSingleton<IGetOnePushEntry, GetOnePushEntry>();

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGenNewtonsoftSupport();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v2", new OpenApiInfo {Title = "GitSyncPusher", Version = "v2"});
    c.EnableAnnotations();
});

builder
    .Services
    .AddHealthChecks()
    .AddMongoDb(
        mongodbConnectionString: builder.Configuration.GetValue<string>("MONGODB_CONNECTION") ??
                                 throw new ValueIsNullException("mongo db connectionString is null"),
        mongoDatabaseName: builder.Configuration.GetValue<string>("MONGODB_DATABASE") ??
                           throw new ValueIsNullException("mongo db databaseName is null"));


var app = builder.Build();

MethodTimeLogger.Logger = app.Logger;

app.UseStaticFiles();
app.UseHttpsRedirection();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger(c => { c.SerializeAsV2 = true; });
    app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v2/swagger.json", "GitSyncPusher"); });
}

app.MapHealthChecks("/healthz", new HealthCheckOptions
{
    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
});

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.UseCors(x =>
    x.AllowAnyMethod()
        .AllowAnyHeader()
        .SetIsOriginAllowed(_ => true)
        .AllowAnyOrigin()
);

await app.RunAsync();