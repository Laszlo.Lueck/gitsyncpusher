using GitSyncPusher.Database;
using LanguageExt;

namespace GitSyncPusher.Services;

public interface IGetOnePushEntry
{
    Task<Option<(PushEntry, Credential)>> GetOnePushEntryOptionAsync(Guid projectId);
}