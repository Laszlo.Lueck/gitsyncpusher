namespace GitSyncPusher.Services;

public record GitConfiguration(string GitDownloadFolder);