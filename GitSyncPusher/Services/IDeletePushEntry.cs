namespace GitSyncPusher.Services;

public interface IDeletePushEntry
{
    Task<DeleteEntryResult> ProcessDeletePushEntry(Guid projectId);
}