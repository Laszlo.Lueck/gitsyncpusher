using LanguageExt;

namespace GitSyncPusher.Services;

public interface IGitHandler
{
    Task<bool> Process(GitConfiguration gitConfiguration, Guid processId, string dockerTag);
}