using GitSyncPusher.Database;
using GitSyncPusher.Endpoints.UpdateEntry;
using static LanguageExt.Prelude;

namespace GitSyncPusher.Services;

public class UpdatePushEntry(ILogger<UpdatePushEntry> logger, IStorageConnector<PushEntry> databaseConnector,
        ICredentialConnector<Credential> credentialConnector)
    : IUpdatePushEntry
{
    public async Task<(int Status, string Message)> Update(UpdatePushEntryDto dto)
    {
        logger.LogInformation("receive an update for project {ProjectId}", dto.ProjectId);
        //split dto to pushEntry and Credential
        var pushEntryOpt = await databaseConnector.GetByProjectIdAsync(dto.ProjectId);
        var credentialOpt = await credentialConnector.GetCredentialByProjectId(dto.ProjectId);


        return await match(
            from pushEntry in pushEntryOpt
            from credential in credentialOpt
            select (pushEntry, credential),
            Some: async tuple =>
            {
                tuple.pushEntry.IsActiveEntity = dto.Active;
                tuple.pushEntry.Name = dto.Name;
                tuple.pushEntry.LastChange = DateTime.Now;

                tuple.credential.LastChangeDate = DateTime.Now;
                tuple.credential.GitDirectoryName = dto.UpdateCredentialDto.GitDirectoryName;
                tuple.credential.BranchName = dto.UpdateCredentialDto.BranchName;
                tuple.credential.GitConfigEmail = dto.UpdateCredentialDto.GitConfigEmail;
                tuple.credential.GitConfigUser = dto.UpdateCredentialDto.GitConfigUser;
                tuple.credential.GitCredentialToken = dto.UpdateCredentialDto.GitCredentialToken;
                tuple.credential.GitCredentialTokenName = dto.UpdateCredentialDto.GitCredentialTokenName;
                tuple.credential.GitProjectUrl = dto.UpdateCredentialDto.GitProjectUrl;
                tuple.credential.GitRepoRemote = dto.UpdateCredentialDto.GitRepoRemote;
                tuple.credential.TemplateFileName = dto.UpdateCredentialDto.TemplateName;
                tuple.credential.GitRepoPushRefSpec = dto.UpdateCredentialDto.GitRepoPushRefSpec;

                var pushEntryStoreResult = await databaseConnector.UpdateOneAsync(tuple.pushEntry);
                var credentialStoreResult = await credentialConnector.UpdateCredential(tuple.credential);
                return (pushEntryStoreResult + credentialStoreResult) switch
                {
                    0 => (304, "no result stored in pushEntry and credential database"),
                    1 => (304, "one result could not be stored in database, see prior log messages"),
                    2 => (200, "successfully stored changes"),
                    _ => (304, "unexpected result received while storing datasets in database")
                };
            },
            None: async () =>
            {
                return await Task.Run(() =>
                {
                    logger.LogWarning("cannot find pushEntry or credential for projectId {ProjectId}", dto.ProjectId);
                    return (404, $" cannot found projectId {dto.ProjectId} for pushEntry or credential in database");
                });
            });
    }
}