namespace GitSyncPusher.Services;

public class PushEntryDatabaseSettings
{
    public string? PushEntryDatabaseName;
    public string? PushEntryConnectionString;
    public string? PushEntryCollectionName;
    public string? PushEntryHistoryCollectionName;
    public string? StoredCredentialsCollectionName;
}