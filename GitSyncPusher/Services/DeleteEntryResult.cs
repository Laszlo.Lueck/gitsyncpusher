namespace GitSyncPusher.Services;

public record DeleteEntryResult(
    Guid ProjectId,
    bool PushEntrySuccess,
    bool CredentialSuccess,
    long DeletedHistories,
    bool HasError,
    string? ErrorMessage);