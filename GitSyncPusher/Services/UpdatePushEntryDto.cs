using GitSyncPusher.Endpoints.UpdateEntry;

namespace GitSyncPusher.Services;

public record UpdatePushEntryDto
(
    Guid ProjectId,
    string Name,
    bool Active,
    UpdateCredentialDto UpdateCredentialDto
)
{
    public static implicit operator UpdatePushEntryDto(UpdateEntryRequest updateEntryRequest) => new(
        Active: updateEntryRequest.Active,
        ProjectId: updateEntryRequest.ProjectId,
        Name: updateEntryRequest.Name,
        UpdateCredentialDto: updateEntryRequest.UpdateCredentialRequest);
}