using LanguageExt;

namespace GitSyncPusher.Services;

public static class GitConfigurationHandler
{
    public static Option<GitConfiguration> GetGitConfiguration(IConfigurationHandler configurationHandler)
    {
        return 
            from downloadFolder in configurationHandler.GetValue<string>("GIT_DOWNLOAD_FOLDER")
            select new GitConfiguration(
                GitDownloadFolder: ((BaseConfigValueString) downloadFolder).Value
            );
    }
}