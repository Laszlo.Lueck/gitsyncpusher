using LanguageExt;

namespace GitSyncPusher.Services;

public interface IConfigurationHandler
{
    Option<BaseConfigValue> GetValue<T>(string key);

    T GetValueUnsafe<T>(Option<TypedBaseConfigValue<T>> value);
}