using LanguageExt;

namespace GitSyncPusher.Services;

public class ConfigurationHandler(ILogger<ConfigurationHandler> logger, IConfiguration configuration)
    : IConfigurationHandler
{
    public T GetValueUnsafe<T>(Option<TypedBaseConfigValue<T>> value)
    {
        if (value.IsNone)
            throw new ArgumentNullException(nameof(value), "value cannot be null");

        return value.Match(
            Some: v => v.Value,
            None: () => throw new ArgumentException("Option value is None")
        ) ?? throw new InvalidOperationException("value should not be null");
    }

    public Option<BaseConfigValue> GetValue<T>(string key)
    {
        var tmpNullable = configuration.GetValue<string>(key);
        if (tmpNullable is not null)
            return typeof(T).Name.ToLower() switch
            {
                "string" => ConvertToStringValue(tmpNullable),
                "int" => ConvertToIntValue(logger, tmpNullable),
                "bool" => ConvertToBoolValue(logger, tmpNullable),
                _ => ConvertNoneLeg(logger, key, typeof(T).Name)
            };
        logger.LogWarning("cannot find value for key {Key} in configuration!", key);
        return Option<BaseConfigValue>.None;

        static Option<BaseConfigValue> ConvertNoneLeg(ILogger logger, string key, string typeName)
        {
            logger.LogWarning("cannot convert type {Type} for key {Key}, because there is no implementation", typeName,
                key);
            return new BaseConfigValue();
        }

        static Option<BaseConfigValue> ConvertToStringValue(string value)
        {
            return new BaseConfigValueString(value);
        }

        static Option<BaseConfigValue> ConvertToIntValue(ILogger logger, string value)
        {
            if (int.TryParse(value, out var intValue))
                return new BaseConfigValueInt(intValue);
            
            logger.LogWarning("cannot convert value {Value} to int, return none", value);
            return Option<BaseConfigValue>.None;
        }

        static Option<BaseConfigValue> ConvertToBoolValue(ILogger logger, string value)
        {
            if (bool.TryParse(value, out var boolValue))
                return new BaseConfigValueBool(boolValue);
            
            logger.LogWarning("cannot convert value {Value} to bool, return none", value);
            return Option<BaseConfigValue>.None;
        }
    }
}

public class BaseConfigValue
{
}

public class TypedBaseConfigValue<T> : BaseConfigValue
{
    public readonly T Value;

    protected TypedBaseConfigValue(T value)
    {
        Value = value;
    }
}

public sealed class BaseConfigValueString : TypedBaseConfigValue<string>
{
    public BaseConfigValueString(string value) : base(value)
    {
    }
}

public sealed class BaseConfigValueInt : TypedBaseConfigValue<int>
{
    public BaseConfigValueInt(int value) : base(value)
    {
    }
}

public sealed class BaseConfigValueBool : TypedBaseConfigValue<bool>
{
    public BaseConfigValueBool(bool value) : base(value)
    {
    }
}