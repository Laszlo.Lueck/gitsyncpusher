namespace GitSyncPusher.Services;

public record CreatePushEntryResult(bool HasError, string? ErrorMessage, Guid? Guid, DateTime? CreateDate);