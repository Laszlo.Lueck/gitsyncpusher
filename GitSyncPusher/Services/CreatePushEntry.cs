using GitSyncPusher.Database;

namespace GitSyncPusher.Services;

public class CreatePushEntry(ILogger<CreatePushEntry> logger, IStorageConnector<PushEntry> pushEntryDatabaseConnector,
        ICredentialConnector<Credential> credentialConnector)
    : ICreatePushEntry
{
    public async Task<CreatePushEntryResult> ProcessNewPushEntry(NewPushEntryDto pushEntryDto)
    {
        logger.LogInformation("first check if the given project name is in the database");

        var checkOpt = await credentialConnector.GetCredentialByProjectName(pushEntryDto.Name);

        return await checkOpt.Match(
            Some: async _ =>
            {
                await Task.Run(() =>
                {
                    logger.LogWarning("pushEntry with name exists, gave up");
                    return false;
                });
                return new CreatePushEntryResult(
                    ErrorMessage:
                    $"pushEntry with name {pushEntryDto.Name} exists in database, cannot create new entry",
                    HasError: true, Guid: null, CreateDate: null);
            },
            None: async () =>
            {
                logger.LogInformation("no similar project found in database, create the new pushEntry");
                var guid = Guid.NewGuid();
                var createDate = DateTime.Now;
                var pushEntry = new PushEntry
                {
                    CreateDate = createDate,
                    ProjectId = guid,
                    LastChange = DateTime.Now,
                    IsActiveEntity = pushEntryDto.Active,
                    Name = pushEntryDto.Name
                };
                var credential = new Credential
                {
                    ProjectId = guid,
                    BranchName = pushEntryDto.CredentialDto.BranchName,
                    GitConfigEmail = pushEntryDto.CredentialDto.GitConfigEmail,
                    GitConfigUser = pushEntryDto.CredentialDto.GitConfigUser,
                    GitCredentialToken = pushEntryDto.CredentialDto.GitCredentialToken,
                    GitCredentialTokenName = pushEntryDto.CredentialDto.GitCredentialTokenName,
                    GitDirectoryName = pushEntryDto.CredentialDto.GitDirectoryName,
                    GitProjectUrl = pushEntryDto.CredentialDto.GitProjectUrl,
                    GitRepoPushRefSpec = pushEntryDto.CredentialDto.GitRepoPushRefSpec,
                    LastChangeDate = DateTime.Now,
                    GitRepoRemote = pushEntryDto.CredentialDto.GitRepoRemote,
                    TemplateFileName = pushEntryDto.CredentialDto.TemplateName,
                    CreateDate = createDate
                };

                await pushEntryDatabaseConnector.WriteOneAsync(pushEntry);
                await credentialConnector.InsertCredential(credential);

                return new CreatePushEntryResult(HasError: false, Guid: pushEntry.ProjectId, ErrorMessage: null,
                    CreateDate: createDate);
            });
    }
}