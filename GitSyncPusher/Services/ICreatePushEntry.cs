using LanguageExt;

namespace GitSyncPusher.Services;

public interface ICreatePushEntry
{

    Task<CreatePushEntryResult> ProcessNewPushEntry(NewPushEntryDto pushEntryDto);

}