using GitSyncPusher.Database;
using LanguageExt;

namespace GitSyncPusher.Services;

public class GetOnePushEntry(ILogger<GetOnePushEntry> logger, IStorageConnector<PushEntry> pushEntryConnector,
        ICredentialConnector<Credential> credentialConnector)
    : IGetOnePushEntry
{
    public async Task<Option<(PushEntry, Credential)>> GetOnePushEntryOptionAsync(Guid projectId)
    {
        logger.LogInformation("try to get information for project {ProjectId}", projectId);
        var pushEntryOpt = await pushEntryConnector.GetByProjectIdAsync(projectId);
        var credentialOpt = await credentialConnector.GetCredentialByProjectId(projectId);

        return from pushEntry in pushEntryOpt
            from credential in credentialOpt
            select (pushEntry, credential);
    }
}