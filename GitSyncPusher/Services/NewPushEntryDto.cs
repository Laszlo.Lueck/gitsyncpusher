using GitSyncPusher.Endpoints.CreateEntry;

namespace GitSyncPusher.Services;

public record NewPushEntryDto
(
    bool Active,
    string Name,
    NewCredentialDto CredentialDto
)
{
    public static implicit operator NewPushEntryDto(CreateEntryRequest createEntryRequest) => new(
        Active: createEntryRequest.Active, Name: createEntryRequest.Name,
        CredentialDto: createEntryRequest.CreateCredentialRequest);
}