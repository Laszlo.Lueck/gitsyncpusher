using GitSyncPusher.Database;
using GitSyncPusher.Exceptions;
using LanguageExt;
using LibGit2Sharp;

namespace GitSyncPusher.Services;

public class GitHandler : IGitHandler
{
    private readonly ILogger<GitHandler> _logger;
    private readonly IStorageConnector<PushEntry> _pushEntryDatabaseConnector;
    private readonly IStorageHistoryConnector<PushEntryHistory> _pushEntryHistoryDatabaseConnector;
    private readonly ICredentialConnector<Credential> _credentialConnector;

    public GitHandler(ILogger<GitHandler> logger, IStorageConnector<PushEntry> pushEntryDatabaseConnector,
        IStorageHistoryConnector<PushEntryHistory> pushEntryHistoryDatabaseConnector,
        ICredentialConnector<Credential> credentialConnector)
    {
        _logger = logger;
        _pushEntryDatabaseConnector = pushEntryDatabaseConnector;
        _pushEntryHistoryDatabaseConnector = pushEntryHistoryDatabaseConnector;
        _credentialConnector = credentialConnector;
    }

    public async Task<bool> Process(GitConfiguration gitConfiguration, Guid processId, string dockerTag)
    {
        _logger.LogInformation("get process information for id {ProcessId}", processId);
        var recordOpt = await _pushEntryDatabaseConnector.GetByProjectIdAsync(processId);
        var credentialOpt = await _credentialConnector.GetCredentialByProjectId(processId);

        var projectDataOpt = from record in recordOpt
            from credential in credentialOpt
            select (record, credential);


        return await projectDataOpt.Match(
            Some: async projectData =>
            {
                try
                {
                    if (!projectData.record.IsActiveEntity)
                    {
                        throw new InactiveEntityException("cannot fulfill the process because the entity is set to inactive state");
                    }
                    
                    if (!string.IsNullOrEmpty(projectData.record.LastDockerTag) &&
                        projectData.record.LastDockerTag == dockerTag)
                    {
                        _logger.LogWarning(
                            "stored dockerTag {DockerTag} ist equivalent to given dockerTag from call, nothing to do",
                            projectData.record.LastDockerTag);
                        throw new EquivalentValuesException(
                            $"dockerTag {projectData.record.LastDockerTag} is equivalent to given dockerTag, nothing to do!");
                    }


                    if (projectData.credential.GitDirectoryName is null)
                    {
                        _logger.LogWarning("git directory name should not be null");
                        throw new ArgumentNullException(nameof(projectData.credential.GitDirectoryName),
                            "git directory name should not be null");
                    }

                    RemoveOldGitDirectoryIfExists(gitConfiguration, projectData.record);

                    CreateNewGitDirectory(gitConfiguration);

                    PushEntryHistory historyRecord = (projectData.record, projectData.credential.GitDirectoryName);

                    var gitRepoPath = CreateGitRepoPath(_logger, projectData.credential, gitConfiguration);

                    _logger.LogInformation("first, create the full path from config");
                    var fullGitPath = new DirectoryInfo(gitConfiguration.GitDownloadFolder).FullName;

                    var combinedPath =
                        $"{fullGitPath}{Path.DirectorySeparatorChar}{projectData.credential.GitDirectoryName}";
                    _logger.LogInformation("look if the stored folder {Folder} exists in the directory",
                        combinedPath);

                    if (!Directory.Exists(combinedPath))
                        throw new DirectoryNotFoundException($"directory {combinedPath} does not exists in context");

                    var templateFilePath =
                        $"{combinedPath}{Path.DirectorySeparatorChar}{projectData.credential.TemplateFileName}";

                    _logger.LogInformation("check if the template file {TemplateFile} is in the path",
                        templateFilePath);
                    if (!File.Exists(templateFilePath))
                        throw new FileNotFoundException($"file in folder {combinedPath} does not exists",
                            projectData.credential.TemplateFileName);

                    var destinationFilePath = $"{combinedPath}{Path.DirectorySeparatorChar}docker-compose.yaml";
                    _logger.LogInformation("lets remove any existing file in {DestinationPath}", destinationFilePath);

                    if (File.Exists(destinationFilePath))
                        File.Delete(destinationFilePath);

                    await ReadReplaceAndWriteToFile(templateFilePath, "[DOCKER_TAG]", dockerTag,
                        destinationFilePath, _logger);

                    _logger.LogInformation("lets create the repository from within the {Folder}", gitRepoPath);
                    var repo = new Repository(gitRepoPath);

                    CommitChanges(projectData.credential, dockerTag, repo, _logger);

                    var remoteOpt = BuildRemoteOpt(projectData.credential, repo, _logger);

                    remoteOpt.Match(
                        Some: remote => PushToGit(projectData.credential, remote, repo, _logger),
                        None: () => throw new ValueIsNoneException(
                            $"cannot fulfill the git process to remote repo {projectData.credential.GitRepoRemote} not found"));

                    await SavePushEntryHistoryInDatabase(historyRecord, _logger, _pushEntryHistoryDatabaseConnector);
                    await UpdateEntryInDatabase(dockerTag, projectData.record, _logger, _pushEntryDatabaseConnector);

                    _logger.LogInformation("processing done, lets cleanup some things");

                    return true;
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception, "an error occured");
                    return false;
                }
                finally
                {
                    _logger.LogInformation("finally remove the git directory");
                    if (Directory.Exists(gitConfiguration.GitDownloadFolder))
                        Directory.Delete(gitConfiguration.GitDownloadFolder, true);
                }
            },
            None: async () =>
            {
                return await Task.Run(() =>
                {
                    _logger.LogWarning("no record for given processId {ProcessID} found in database, gave up",
                        processId);
                    return false;
                });
            });
    }


    private static void PushToGit(Credential credential, Remote remote, IRepository repo, ILogger logger)
    {
        logger.LogInformation("remote {Remote} found, lets process", remote.Name);
        var pushOptions = new PushOptions
        {
            CredentialsProvider = (_, _, _) =>
                new UsernamePasswordCredentials
                {
                    Username = credential.GitCredentialTokenName,
                    Password = credential.GitCredentialToken
                }
        };

        logger.LogInformation("push all the changes to remote!");
        repo.Network.Push(remote, credential.GitRepoPushRefSpec, pushOptions);
    }


    private static async Task UpdateEntryInDatabase(string dockerTag, PushEntry record, ILogger logger,
        IStorageConnector<PushEntry> pushEntryDatabaseConnector)
    {
        logger.LogInformation("lets update the dataset in database");

        record.LastChange = DateTime.Now;
        record.LastDockerTag = dockerTag;
        await pushEntryDatabaseConnector.UpdateOneAsync(record);
    }

    private static async Task SavePushEntryHistoryInDatabase(PushEntryHistory entryHistory, ILogger logger,
        IStorageHistoryConnector<PushEntryHistory> historyStorageConnector)
    {
        logger.LogInformation("write the current record in the history collection");
        await historyStorageConnector.WriteOneAsync(entryHistory);
    }


    private static Option<Remote> BuildRemoteOpt(Credential credential, IRepository repo, ILogger logger)
    {
        logger.LogInformation("try to get the configured branch {BranchName} from remote",
            credential.GitRepoRemote);
        return repo.Network.Remotes[credential.GitRepoRemote];
    }


    private static void CommitChanges(Credential credential, string dockerTag, IRepository repo, ILogger logger)
    {
        logger.LogInformation("lets create the git signature to commit the changes");
        var signature = new Signature(credential.GitConfigUser, credential.GitConfigEmail,
            DateTimeOffset.Now);

        logger.LogInformation("add the changes to the local branch");
        Commands.Stage(repo, "*");

        logger.LogInformation("commit the changes with a message");
        repo.Commit(
            $"[AUTO] update project {credential.GitDirectoryName} set latest docker tag {dockerTag} to compose yaml",
            signature,
            signature);
    }

    private static async Task ReadReplaceAndWriteToFile(string inputPath, string placeholder, string replacement,
        string outputPath, ILogger logger)
    {
        try
        {
            logger.LogInformation("write the template file from path {TemplatePath}", inputPath);
            logger.LogInformation("write the result to destination path {DestinationPath}", outputPath);

            var lines = await File.ReadAllLinesAsync(inputPath);
            var outputLines = lines.Select(line =>
                line.Contains(placeholder) ? line.Replace(placeholder, replacement) : line);
            await File.WriteAllLinesAsync(outputPath, outputLines);
        }
        catch (Exception exception)
        {
            logger.LogError(exception, "an error occured");
            throw;
        }
    }

    private static string CreateGitRepoPath(ILogger logger, Credential credential, GitConfiguration gitConfiguration)
    {
        logger.LogInformation("lets clone the project in the directory");
        var gitRepoPath = Repository.Clone(credential.GitProjectUrl,
            gitConfiguration.GitDownloadFolder,
            new CloneOptions {BranchName = credential.BranchName});
        logger.LogInformation("after cloning, we get the repo path {RepoPath}", gitRepoPath);
        return gitRepoPath;
    }

    private void CreateNewGitDirectory(GitConfiguration gitConfiguration)
    {
        if (Directory.Exists(gitConfiguration.GitDownloadFolder)) return;

        _logger.LogInformation(
            "folder for git processing {DownloadFolder} does not exists, lets create them!",
            gitConfiguration.GitDownloadFolder);
        Directory.CreateDirectory(gitConfiguration.GitDownloadFolder);
    }

    private void RemoveOldGitDirectoryIfExists(GitConfiguration gitConfiguration, PushEntry record)
    {
        _logger.LogInformation("lets work with the project {ProjectName}",
            record.Name ?? "no name exposed");

        if (!Directory.Exists(gitConfiguration.GitDownloadFolder)) return;

        _logger.LogWarning(
            "an old git repository folder exists, possible not correctly clean up from last run, lets remove them");
        Directory.Delete(gitConfiguration.GitDownloadFolder, true);
    }
}