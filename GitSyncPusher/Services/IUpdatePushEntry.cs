using GitSyncPusher.Endpoints.UpdateEntry;

namespace GitSyncPusher.Services;

public interface IUpdatePushEntry
{
    Task<(int Status, string Message)> Update(UpdatePushEntryDto request);
}