﻿using GitSyncPusher.Endpoints.CreateEntry;

namespace GitSyncPusher.Services;

public record NewCredentialDto(string BranchName, string GitConfigUser, string GitConfigEmail,
    string GitCredentialTokenName, string GitCredentialToken, string GitRepoRemote, string GitRepoPushRefSpec,
    string GitProjectUrl, string GitDirectoryName, string TemplateName)
{

    public static implicit operator NewCredentialDto(CreateCredentialRequest credentialRequest) => new(
        BranchName: credentialRequest.BranchName, GitConfigUser: credentialRequest.GitConfigUser,
        GitConfigEmail: credentialRequest.GitConfigEmail,
        GitCredentialTokenName: credentialRequest.GitCredentialTokenName,
        GitCredentialToken: credentialRequest.GitCredentialToken, GitRepoRemote: credentialRequest.GitRepoRemote,
        GitRepoPushRefSpec: credentialRequest.GitRepoPushRefSpec, GitProjectUrl: credentialRequest.GitProjectUrl,
        GitDirectoryName: credentialRequest.GitDirectoryName, TemplateName: credentialRequest.TemplateFileName);
}
