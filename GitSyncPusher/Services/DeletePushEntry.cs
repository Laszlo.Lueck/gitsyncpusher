using GitSyncPusher.Database;

namespace GitSyncPusher.Services;

public class DeletePushEntry(ILogger<DeletePushEntry> logger, IStorageConnector<PushEntry> storageConnector,
        ICredentialConnector<Credential> credentialConnector,
        IStorageHistoryConnector<PushEntryHistory> storageHistoryConnector)
    : IDeletePushEntry
{
    public async Task<DeleteEntryResult> ProcessDeletePushEntry(Guid projectId)
    {
        long deleteCredentialResponse = 0;
        long deleteProjectResponse = 0;
        long deleteHistoryResponse = 0;

        try
        {
            logger.LogInformation("try to remove push entry with id {PushEntryId}", projectId);
            deleteCredentialResponse = await credentialConnector.DeleteCredentialByProjectId(projectId);
            deleteProjectResponse = await storageConnector.DeleteOneAsync(projectId);
            deleteHistoryResponse = await storageHistoryConnector.DeleteAllByProjectId(projectId);

            return new DeleteEntryResult(ProjectId: projectId, PushEntrySuccess: deleteProjectResponse == 1,
                CredentialSuccess: deleteCredentialResponse == 1, DeletedHistories: deleteHistoryResponse,
                HasError: false, ErrorMessage: null);
        }
        catch (Exception exception)
        {
            logger.LogError(exception, "an error occured");
            return new DeleteEntryResult(ProjectId: projectId, HasError: true, ErrorMessage: exception.Message,
                DeletedHistories: deleteHistoryResponse, PushEntrySuccess: deleteProjectResponse == 1,
                CredentialSuccess: deleteCredentialResponse == 1);
        }
    }
}