namespace GitSyncPusher.Services;

public class GitHandlerResult
{
    public bool HasError;
    public string? ErrorMessage;
}